import os
from MQTT import client

## ~~ STORED DATA ~~ ########################################################################################################################

#       container_data = {'container_id' : ['container_name', 'container_host', 'container_port]}
container_data = dict()

def test(a,b):
    os.popen(f'docker run --name yes -p 4000:4000 emqx/emqx')
    message = str(a)+' '+str(b)
    return message

def help():
    # GARRANTZITSUA: Flask-i ematen diogun testua, HTML bidez renderizatuko denez, honen formatuarekin eman ahal diogu forma
    message = '''<p>Following this messsage you will find a list with the command and what they do:</p>
                 <p> ~ startBroker(container_name, host_port, container_port): </p>
                 <p> Through Docker it will start a container executing EMQX MQTT broker with the desired container_name, host_port and container_port</p>
                 <p> ~ brokerStatus(container_name): </p>
                 <p> Gives info about all the containers that have been initialized during the session, if we want information about all the containers
                    in use or initialized, use sowContainers(Active/All)</p>'''
    return message

## ~~ BROKER MANAGEMENT THROUGH DOCKER ~~ ###################################################################################################
def startBroker(container_name, container_port):

    os.popen(f'docker run --name {container_name} -p {container_port}:{container_port} emqx/emqx:4.3.7')
    message = f'Container has been created with name: {container_name}, and port: {container_port}'
    return message


def brokerStatus(container_name):
    i = 0
    try:   
        for name in list(container_data.keys()):
            if container_name == name:   
                #ONDO JARRI DATUEN ESTRUKTURA 
                message = f'''The broker container is up, with ID: {container_data[container_name][0]}, 
                              Container's name is: {container_name}
                              Broker's host port: {container_data[container_name][1]}, 
                              Broker's container port: {container_data[container_name][2]}'''       # Honela lortzen dugu lista bat,  nahi ditugun elementuekin
            i+=1
        return message
    except:
        message = 'The broker is down, it has to be started.'
        return message

def stopBroker(container_name):
    message = 'Broker has been stopped, wait a little before finishing it if you would like to do so'
    os.popen(f'docker stop {container_name}')
    return message

def finishBroker(container_name):

    message_stream = os.popen(f'docker rm {container_name}')
    message_list = str(message_stream.read()).split()
    if message_list == list():
        message = f'Container has to be stopped first, type "stopBroker({container_name})"'
    else:
        message = 'Broker was stopped and the container finished'
    return message
 
def executeCmd(str):
    message = os.popen(f'docker exec emqx {str}')
    return message

## ~~ MONGO MANAGEMENT THORUGH DOCKER ~~ ##################################################################################################################

def startDatabase(container_name):
    os.popen(f'docker run --name {container_name} mongo')
    message = f'The latest MongoDB image has been started with name: {container_name} and listening to standard MongoDB port: 27017'
    return message

## ~~ MMA/HERMES MANAGEMENT TROUGH DOCKER ~~ #############################################################################################################
def startClient():
    #docker run -it -p 6000:6000 -e CLIENT_NAME=client1 -e SESSION_TYPE=None -e USERDATA=None -e TRANSPORT_FORM=tcp -e MQTT_BROKER=broker.emqx.io  -e BROKER_PORT=1883  -e KEEP_ALIVE=10 -e BIND_ADDRESS=None -e LOOP_TIME=None  -e MONGO_URL="mongodb+srv://user1:user1@cluster0.ovmwt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority" -e MONGO_PORT=None -e MONGO_USER=None -e MONGO_PASS=None hermes
    message = 'Client goes here'
    return message

## ~~ DOCKER MANAGEMENT ~~ ##################################################################################################################
def setup():
    os.popen('docker version')
    message = 'The docker daemon has been started.'
    return message

def startDocker():
    try:
        os.popen(f'usermod -aG docker root')
        # HEMEN GAUDE
        message = 'yes'
        return message
    except:
        message = 'no'
        return message

def showContainers(operation):
    if operation == 'Active':
        message_stream = os.popen('docker ps')
        message = message_stream.read()
        return message
    elif operation == 'All':
        message_stream = os.popen('docker ps -a')
        message = message_stream.read()
        return message
    else:
        message = 'Only "Active" or "All" are taken as arguments.'
        return message

## ~~ AUX FUNCTIONS ~~ ######################################################################################################################

## ~~ TEST MAIN ~~ ##########################################################################################################################

## ~~ BESTE APUNTEAK ~~ #####################################################################################################################

# Docker buruzko komando garrantzitsuak:
#       docker stop container_name
#       docker rm container_name        -> Honela gelditu eta hil egiten dugu kontainer bat