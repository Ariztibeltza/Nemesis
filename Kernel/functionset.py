import os
import subprocess
import socket
import time
import json

from string import ascii_lowercase

from email import message
from MQTT import client

## ~~ STORED DATA ~~ ########################################################################################################################

socket_dict = dict()
prometheus_targets = dict()
part_data = list()

## ~~ GENERIC FUNCTIONS ~~ ##################################################################################################################
def test(a,b):
    """a eta b string-ak hartue giten ditu, emqx/emqx container bat sortzen du eta bi hauen konbinaketa bueltatzen du string moduan

    Args:
        a (string): string bat
        b (string): string bat

    Returns:
        message (string): a stringaren eta b stringaren batura

    Structure:
        Lehenengo komandoaren bidez emqx/emqx containerra irekitzen du
        message sortu egiten du a + b eginez
    """
    os.popen(f'docker run --name yes -p 4000:4000 emqx/emqx')
    message = str(a)+' '+str(b)
    return message

def help():
    """Erabili ahal diren komandoei buruzko informazioa ematen digu

    Args:
        None

    Returns:
        message: erabili daitezkeen komando guztien informazioa ematen digu
    """
    # GARRANTZITSUA: Flask-i ematen diogun testua, HTML bidez renderizatuko denez, honen formatuarekin eman ahal diogu forma
    message = '''<p>Following this messsage you will find a list with the command and what they do:</p>
                 <p> ~ startBroker(container_name, host_port, container_port): </p>
                 <p> Through Docker it will start a container executing EMQX MQTT broker with the desired container_name, host_port and container_port</p>
                 <p> ~ brokerStatus(container_name): </p>
                 <p> Gives info about all the containers that have been initialized during the session, if we want information about all the containers
                    in use or initialized, use sowContainers(Active/All)</p>'''
    return message

def initialize():
    os.popen(' docker run -p 9100:9100 prom/node-exporter')

    # With this we will erase all data from the .json file
    open('/nemesis-vol/targets.json','w').close()
    # Hemen ikusi behar dugu nola jaso datuak zuzenean honetatik eta sarearen beste elementuetatik
    

def ip():
    command = ['ipconfig']
    message_raw = os.popen(command[0])
    return ip_prompt_parser(message_raw)

## ~~ BROKER MANAGEMENT THROUGH DOCKER ~~ ###################################################################################################
def startEmqx(container_name, container_port):
    """conainer_name eta container_port hartu eta message bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna
        container_port (string): string bat sorut nahi den containerraren porta ematen diguna

    Returns:
        message (string): containerraren sorrera eta container_name eta container_port bueltatzen duen stringa

    Structure:
        Sistemari comando bidez container_name eta container_port ezaugarriak dituen emqx/emqx:4.3.7 containerra sortzen du
    """
    os.popen(f'docker run --net bridge --name {container_name} -p {container_port}:{container_port} emqx/emqx:4.3.7')
    message = f'Container has been created with name: {container_name}, and port: {container_port}'
    return message

def stopEmqx(container_name):
    """conainer_name hartu eta message bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna

    Returns:
        message (string): containerraren geldiera adierazten du

    Structure:
        Sistemari comando bidez container_name containerra gelditzeko esaten zaio 
    """
    message = 'Broker has been stopped, wait a little before finishing it if you would like to do so'
    os.popen(f'docker stop {container_name}')
    return message

def finishEmqx(container_name):
    """conainer_name hartu eta message bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna

    Returns:
        message (string): containerraren itxiera adierazten du

    Structure:
        Sistemari comando bidez container_name containerra hiltzeko esaten zaio 
    """
    message_stream = os.popen(f'docker rm {container_name}')
    message_list = str(message_stream.read()).split()
    if message_list == list():
        message = f'Container has to be stopped first, type "stopBroker({container_name})"'
    else:
        message = 'Broker was stopped and the container finished'
    return message
 
def executeCmd(cmd):
    # No se si esto funciona muy bien, casi mejor quitarlo
    """cmd hartu eta message_pre parseatuta bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna

    Returns:
        message (string): containerraren komandoaren exekuziotik bueltatzen den mezua adierazten du

    Structure:
        command lista bat bezala definitzen dugu
        message_raw objektu berezi bat da, subprocess prozesutik datorrena eta honen exekuziotik datorren erantzuna buletatzen digu
        message_pre formatoa byte formara bueltatzen digu eta hau irakurri etadekodetu beharko dugu
        cmd_prompt_parser message_pre-ren fromatoa egokia egiten digu html-ri emateko eta argi ikusteko pantailan
    """
    #message = os.popen(f'docker exec emqx {cmd}')
    command = ['docker', 'exec', 'emqx', cmd]
    message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
    message_pre = message_raw.stdout.read().decode("utf-8")
    return cmd_prompt_parser(message_pre)

## ~~ MONGO MANAGEMENT THORUGH DOCKER ~~ ##################################################################################################################

def startMongo(container_name):
    """conainer_name hartu eta message bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna

    Returns:
        message (string): containerraren sorrera, container_name eta konektatzen den port finko batera bueltatzen duen stringa

    Structure:
        Sistemari comando bidez container_name ezaugarria ematen dio, eta 27017 portuan konektatzne da Mongo Atlas erabiltzeko.
        https://account.mongodb.com/account/login?n=%2Fv2%2F60ec060549610b7d938e08c3&nextHash=%23clusters
    """
    os.popen(f'docker run --name {container_name} mongo')
    message = f'The latest MongoDB image has been started with name: {container_name} and listening to standard MongoDB port: 27017'
    return message

## ~~ MMA/HERMES MANAGEMENT TROUGH DOCKER ~~ #############################################################################################################

def startHermes(container_name, container_port, client_name, session_type, userdata, transport_form, mqtt_broker, broker_port, keep_alive, bind_address, loop_time, sub_list, mongo_url, mongo_port, mongo_user, mongo_pass):
    """container_name, container_port, client_name, session_type, userdata, transport_form, mqtt_broker, broker_port, keep_alive, bind_address, 
       loop_time, sub_list, mongo_url, mongo_port, mongo_user, mongo_pass hartu eta message bueltatzen du

    Args:
        container_name (string): string bat sortu nahi den containerraren izena ematen diguna
        container_port (string): string bat sorut nahi den containerraren porta ematen diguna
        client_name (string): string bat clientearen iena ematen duena, MongoDB barnean erabiltzeko
        session_type (string):
        userdata (string):
        transport_form (string):
        mqtt_broker (string):
        broker_port (string):
        keep_alive (string):
        bind_adress (string):
        loop_time (string):
        sub_list (string):
        mongo_url (string):
        mongo_port (string):
        mongo_user (string):
        mongo_pass (string):

    Returns:
        message (string): containerraren sorrera adierazten duen mensajea

    Structure:
        Sistemari comando bidez container_name eta container_port ezaugarriak dituen emqx/emqx:4.3.7 containerra sortzen du
    """
    
                                                                                                                    # HEMEN GAUDE   
    
    
    # Here we have 16 variables, and we have to be careful with the sub_list variable, as
    #       -> We are assuming that it is not a list, it is a list that will 
    #          be converted in the container itself, we just receive all the environmental variables as str
    os.popen(f'docker run -it -p {container_port}:{container_port} --name {container_name} -e CLIENT_NAME={client_name} -e SESSION_TYPE={session_type} -e USERDATA={userdata} -e TRANSPORT_FORM={transport_form} -e MQTT_BROKER={mqtt_broker}  -e BROKER_PORT={broker_port}  -e KEEP_ALIVE={keep_alive} -e BIND_ADDRESS={bind_address} -e LOOP_TIME={loop_time}  -e SUBSCRIPTION_LIST={sub_list} -e MONGO_URL={mongo_url} -e MONGO_PORT={mongo_port} -e MONGO_USER={mongo_user} -e MONGO_PASS={mongo_pass} hermes')
    message = 'Client goes here'
    return message

## ~~ DOCKER MANAGEMENT ~~ ##################################################################################################################

def showContainers(operation):
    """operation hartu eta message_pre parseatuta edo message bueltatzen du

    Args:
        operation (string): string bat jaso nahi den informazioaren araberakoa

    Returns:
        message (string): eskatutko informazioaren araberako string bat buetatzen du

    Structure:
        operation == 'Active' izanda bakarrik funtzionatzen ari diren containerrak erakusten dizkigu
        operation == 'All' izanda funtzionatzen zein STOP forman dauden kontainerrak erakusten ditu
        else hartuta operation ezegoki bat eman zaio
    """
    if operation == 'Active':
        command = ['docker', 'ps']
        message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
        message_pre = message_raw.stdout.read().decode("utf-8")
        return cmd_prompt_parser(message_pre)
    elif operation == 'All':
        command = ['docker', 'ps', '-a']
        message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
        message_pre = message_raw.stdout.read().decode("utf-8")
        return cmd_prompt_parser(message_pre)
    else:
        message = 'Only "Active" or "All" are taken as arguments.'
        return message


def createNetwork(network_name):
    os.popen(f'docker network create {network_name}') 
    message=f'A network has been created with name: {network_name}'
    return message

def connectContainerNetwork(container_name, network_name):
    os.popen(f'docker network connect {network_name} {container_name}')
    message=f'The container: {container_name} has been connected to network: {network_name}'
    return message

def networkListing():
    """message_pre parseatuta bueltatzen du

    Args:
        None

    Returns:
        message: dockerren network ezberdinen elementuak erakusten dira

    Structure:
        command lista bat bezala definitzen dugu
        message_raw objektu berezi bat da, subprocess prozesutik datorrena eta honen exekuziotik datorren erantzuna buletatzen digu
        message_pre formatoa byte formara bueltatzen digu eta hau irakurri etadekodetu beharko dugu
        cmd_prompt_parser message_pre-ren fromatoa egokia egiten digu html-ri emateko eta argi ikusteko pantailan
    """
    command = ['docker', 'network', 'ls']
    message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
    message_pre = message_raw.stdout.read().decode("utf-8")
    return cmd_prompt_parser(message_pre)

def networkInspection(network_name):
    command = ['docker', 'network', 'inspect', network_name]
    message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
    message_pre = message_raw.stdout.read().decode("utf-8")
    return network_data_parser(message_pre)

def containerIP(container_id):
    command = ['docker', 'inspect',container_id]
    message_raw = subprocess.Popen(command, stdout=subprocess.PIPE)
    message_pre = message_raw.stdout.read().decode("utf-8")
    return conatiner_ip_parser(message_pre,container_id)

## ~~ CONNECTION FUNCTIONS ~~ ###############################################################################################################

# Interesgarria egingo zaigu beste leku batean dagoen container bati konektatzea, hau dela eta ikusiko dugu honakoa oso erabilgarria egingo
# zaigula

#   ->  https://stackoverflow.com/questions/46251790/how-to-access-a-docker-container-from-another-lan-pc-within-a-home-network

def connect(container_name,ip_add, port):
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        clientSocket.connect((str(ip_add), int(port)))
        socket_dict[container_name] = [ip_add,port, clientSocket]
        message = f'Connected to IPv4: {ip_add} through port: {port}'
    except:
        message = f'Impossible to connect to IPv4: {ip_add} through port: {port}'
    return message

def disconnect(container_name, ip_add, port):
    return None

def connection_data():
    return renewData()

## ~~ DATA FUNCTIONS ~~ #####################################################################################################################

# Informazioa partekatzeko honako tutoriala jarraitu:
#       ->  https://docs.docker.com/network/network-tutorial-standalone/

def renewData():
    message=str()

    if socket_dict != {}:
        for i in range (0,len(socket_dict.keys())):
            try:
                data = socket_dict[socket_dict.keys()[i]][2].recv(1024).decode()
                message += f'<p> Data of {socket_dict.keys()[i]}: {data} </p>'
            except:
                message += f'<p> Problem with {socket_dict.keys()[i]}'
    else:
        message = 'There is no connected container/application'

    return message

## ~~ MONITORING FUNCTIONS ~~ ###############################################################################################################

def monitor():
    os.popen(' docker run --name nemesis_prometheus --publish published=9090,target=9090,protocol=tcp -v nemesis-vol:/nemesis-vol monitoring')
    os.popen(' docker run --name nemesis_grafana -d -p 3000:3000 grafana/grafana-oss')

def promTargets(target_ip,target_port,job_name):

    file = open('/nemesis-vol/targets.json','r')
    
    try:
        pre_data = json.load(file)
    except:
        pre_data = []

    file = open('/nemesis-vol/targets.json','w')
    target = {"targets":[f"{target_ip}:{target_port}"],
              "labels":{"env":"prod","job":job_name}}      
    pre_data.append(target)
    json.dump(pre_data,file)
    file.close()

    message = f'Target: "{target_ip}:{target_port}" has been added'

    return message

## ~~ NEMESIS EXTERNAL MANAGEMENT ~~ ########################################################################################################

## ~~ AUX FUNCTIONS ~~ ######################################################################################################################

def ip_prompt_parser(message_raw):
    # HAU WINDOWSERAKO BAKARRIK FUNTZIONATZEN DU
    message = str()
    message_list_final = list()
    message_list = message_raw.read().split()
    
    for element in message_list:
        if element != '.':
            message_list_final.append(element)
    
    for i in range(0, len(message_list_final)):
        if message_list_final[i] == 'IPv4.':
            message = message_list_final[i+2]
    return message
    
def cmd_prompt_parser(message_pre):
    message = str()
    message_list = message_pre.split('\n')
    for element in message_list:
        message += '<p>'+element+'<p>'
    return message

def network_data_parser(message_pre):
    message=str()
    name_list = list()
    final_name = str()
    ip_list = list()
    final_ip = str()
    containers = False
    container_data_list = list()

    message_list = message_pre.split('\n')
    
    for element in message_list:
        if element.split(':')[0] == '        "Containers"':
            containers = True
        elif element.split(':')[0] == '        "Options"':
            containers = False
        elif containers == True:
            container_data_list.append(element)

    for element in container_data_list:
        if element.split(':')[0] == '                "Name"':
            name_list.append(element.split(':')[1])
        elif element.split(':')[0] == '                "IPv4Address"':
            ip_list.append(element.split(':')[1])

    for i in range (0, len(name_list)):
        for character in name_list[i]:
            if character in ascii_lowercase or character in '0123456789':
                final_name += character
        for character in ip_list[i]:
            if character in '0123456789' or character == '.' or character == '/':
                final_ip += character
        name_list[i]=final_name
        ip_list[i] = final_ip
        
        final_name = str()
        final_ip = str()
    
    for j in range(0, len(name_list)):
        message += '<p>'+name_list[j]+' : '+ip_list[j]+'<p>'

    return message

def conatiner_ip_parser(message_pre,container_id):
    message_list = message_pre.split('\n')

    for element in message_list:
        identif = element.split(':')
        if identif[0] == '            "IPAddress"':
            message = f'Container with ID: "{container_id}" has IPAddress: {identif[1]}'
    
    return message

## ~~ BESTE APUNTEAK ~~ #####################################################################################################################

# Docker buruzko komando garrantzitsuak:
#       docker stop container_name
#       docker rm container_name        -> Honela gelditu eta hil egiten dugu kontainer bat