import socket
import time

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind(("localhost",10000))
serverSocket.listen(1)

print(serverSocket.getsockname())

while True:
    print('waiting for connection')
    connection, client_address = serverSocket.accept()
    try:
        (clientConnected, clientAddress) = serverSocket.accept()
        print("Accepted a connection request from %s:%s"%(clientAddress[0], clientAddress[1]))
        time.sleep(1)
        clientConnected.send("Hello Client")
    except:
        None