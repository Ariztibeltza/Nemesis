import json

def promTargets(target_ip,target_port,job_name):

    file = open('Kernel/TESTS/PROM_TEST/targets.json','r')
    
    try:
        pre_data = json.load(file)
    except:
        pre_data = []

    file = open('Kernel/TESTS/PROM_TEST/targets.json','w')

    target = {"targets":[f"{target_ip}:{target_port}"],
              "labels":{"env":"prod","job":job_name}}
            
    pre_data.append(target)
    json.dump(pre_data,file)

    file.close()

## ~~ MAIN ~~ #################################################################

promTargets("1.0.0.0",'9090','prom')
promTargets("1.0.197.0",'3000','nemesis')
promTargets("1.987.0.0",'4000','hermes')

