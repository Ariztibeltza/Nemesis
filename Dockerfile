# For more information, please refer to     https://aka.ms/vscode-docker-python
# Irudiaren izena aldatzeko                 https://stackoverflow.com/questions/25211198/docker-how-to-change-repository-name-or-rename-image
# Fitxategia nola egiteko info:             https://www.balena.io/docs/learn/develop/dockerfile/
#FROM python:3.8-slim-buster
FROM ubuntu

EXPOSE 5000
SHELL ["/bin/bash", "-c"]

#Web orrian esaten dutenaren arabera
ENV VAR1=10

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

USER root

# Install pip requirements
#COPY requirements.txt .

#Here we install everything we need in order to have to have Docker in Docker
RUN apt update -y
RUN apt upgrade -y
RUN apt-get install -y vim 
RUN apt-get install -y python
RUN apt-get install -y python3-pip
RUN apt install -y docker.io
RUN pip install flask 
RUN pip install pyyaml
RUN pip install gunicorn
RUN pip install paho-mqtt

#We have been getting some trouble with iptables once we are inside ubuntu, thus, we need to solve them. We can do the following:
#             https://unix.stackexchange.com/questions/459206/list-ip-tables-in-docker-container          
#RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY . /app

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]