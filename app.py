#   $env:FLASK_APP = 'app.py'
#	flask run --host=0.0.0.0 --port=80
#   Ondoren, amatatzeko Ctrl+C klikatu

#   DATA -> https://flask.palletsprojects.com/en/2.0.x/quickstart/#a-minimal-application
#               Remember that for deployment we have to look here -> https://flask.palletsprojects.com/en/2.0.x/deploying/

import time

from flask import Flask
from flask import render_template
from flask import request

from Kernel import kernel

app = Flask(__name__)  

## ~~ INDEX
@app.route("/")
def index():
    kernel.debug('initialize()')
    return render_template('index.html.jinja')                 #Honekin html kodea renderizatuko dugu, zuzenean

## ~~ LOGIN
@app.route("/login", methods = ['POST', 'GET'])
def login():
    if request.method == 'POST':
        username = request.form.to_dict().get('username')
        password = request.form.to_dict().get('password')
        return render_template('login.html.jinja', username = username, password = password)
    else:
        return render_template('login.html.jinja')

## ~~ GUI
@app.route("/gui", methods = ['POST','GET'])
def gui():
    if request.method == 'POST':
        execute_u_r(request.form.to_dict())
        return render_template('gui.html.jinja')
    else:        
        return render_template('gui.html.jinja')

## ~~ CMDPROMPT
@app.route("/cmdprompt", methods = ['POST','GET'])           #Hau sortu dugu cmd-tik lortu ditugun datuak maneiatzeko
def data():
    #As we need to get data, we might wnt to do look at this:
    #                   https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request
    #           ->      https://www.askpython.com/python-modules/flask/flask-forms
    if request.method == 'POST':
        [cmd_id,cmd_args,error,cmd_data,message] = execute_cmd(request.form.to_dict())                   #message = message zuzneud beharko dugu
        return render_template('cmd.html.jinja', cmd_id = cmd_id, cmd_data = cmd_data, error = error, message = message)
    else:
        return render_template('cmd.html.jinja')  

## ~~ IMPLEMENTED FUNCTIONS ~~ #########################################################################################################################

## ~~ COMMAND EXECUTION FUNCTIONS

## HORRELA LORTU DEZAKEGU ImmutableMultiDict-en barnean dauden elementu guztiak
def execute_u_r(form_dict):
    #execute_user_request

    # Honek ahalbidetzen digu erabiltzailearen eskaer abereziak maneiatzea commando formatoa ez badu erabiltzen hau da, HTML-tik input moduan 
    # hartutako mensajeak erabiliz

    kernel.execute_u_r(form_dict)

def execute_cmd(form_dict):
    for element in form_dict:
        [cmd_id,cmd_args,error,cmd_data,message]=kernel.debug(list(form_dict.get(element)))
    return cmd_id,cmd_args,error,cmd_data,message


## ~~ MAIN ~~ ##########################################################################################################################################

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 5000, debug = True)