Interesgarria dirudigu containerraren barneko fitxategiak sortzea, bereziki beste kontainerren konfiguraketa egiteko.

Beraz, halakoak egiteko beharrezkoa egingo zaigu fitxategia gure containerrarne barnean non dagoen ikustea.